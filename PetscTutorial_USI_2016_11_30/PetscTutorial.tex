\documentclass{beamer}

\usepackage[backend=bibtex,maxnames=100]{biblatex} %from macports texlive-bibtex-extra
\addbibresource{refs.bib}
\renewcommand{\footnotesize}{\ttfamily\tiny}

\usepackage{xcolor}
\usepackage{amsmath}

\definecolor{greenmod}{rgb}{0,0.6,0}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{listings}
\definecolor{verylightgray}{gray}{0.95}
\definecolor{somewhatdarkgray}{gray}{0.3}

% General/default listing settings are for C code
\lstset{
  language=C,
  basicstyle=\ttfamily,
  escapechar=\$,        % Also hardcoded in /bin/main/mapnameslatex.py
  commentstyle=\color{somewhatdarkgray}\ttfamily,
  showstringspaces=false,
  basewidth=0.5em,      % For consistent spacing with inline listings
  breaklines=true,
  backgroundcolor=\color{verylightgray},
  frame=single,
  framexleftmargin= 3px,
  framexrightmargin= 3px,
  rulecolor=\color{lightgray}
}

% http://tex.stackexchange.com/questions/226929/making-algorithm2e-environments-overlay-aware-in-beamer
\resetcounteronoverlays{algocf}

\usetheme{Madrid}
\setbeamertemplate{navigation symbols}{} % Remove the navigation symbols
\setbeamertemplate{sections/subsections in toc}[default] %Turn off ugly toc numbering
\setbeamertemplate{itemize subitem}[triangle]
\setbeamertemplate{itemize item}[triangle]
\setbeamertemplate{enumerate items}[default]
%\setbeamercovered{transparent}
\usepackage{appendixnumberbeamer}
\defbeamertemplate{section page}{customsection}[1][]{%
  \begin{centering}
    {\usebeamerfont{section name}\usebeamercolor[fg]{section name}#1}
    \vskip1em\par
    \begin{beamercolorbox}[sep=12pt,center]{part title}
      \usebeamerfont{section title}\insertsection\par
    \end{beamercolorbox}
  \end{centering}
}
\defbeamertemplate{subsection page}{customsubsection}[1][]{%
  \begin{centering}
    {\usebeamerfont{subsection name}\usebeamercolor[fg]{subsection name}#1}
    \vskip1em\par
    \begin{beamercolorbox}[sep=8pt,center,#1]{part title}
      \usebeamerfont{subsection title}\insertsubsection\par
    \end{beamercolorbox}
  \end{centering}
}
\AtBeginSection{\frame{\sectionpage}}
%\AtBeginSubsection{\frame{\subsectionpage}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\graphicspath{{./images/}{../PetscTutorial/}{../PetscTutorial/images/}}

% clashes with beamer
%\usepackage[colorlinks=true]{hyperref}

% Customize hyperref here
\hypersetup{colorlinks=true,linkcolor=black}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\mattwo}[4]{\left[ \begin{array}{cc}  #1 & #2 \\ #3 & #4 \end{array}  \right]}
\newcommand{\matthree}[9]{\left[ \begin{array}{ccc}  #1 & #2 & #3 \\ #4 & #5 & #6 \\ #7 & #8 & #9 \end{array}  \right]}
\newcommand{\colvectwo}[2]{\left[ \begin{array}{c} #1 \\ #2 \end{array}\right]}
\newcommand{\colvecthree}[3]{\left[ \begin{array}{c} #1 \\ #2 \\ #3 \end{array}\right]}

  \newcommand{\exercisecolor}[1]{\textcolor{green}{#1}}
  \newcommand{\walkthroughcolor}[1]{\textcolor{magenta}{#1}}

%\newcommand{\PETSc}{\textsc{PETSc}}

% Actually prefer the plain text to the small caps
\newcommand{\PETSc}{{PETSc}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\author[Patrick Sanan]{Patrick Sanan \\ \href{mailto:patrick.sanan@erdw.ethz.ch}{patrick.sanan@[usi.ch,erdw.ethz.ch]}}
\institute[USI Lugano / ETH Z\"{u}rich] 
{
USI Lugano / ETH Z\"{u}rich\\
%\vspace{10pt}
%With the \hspace{2px}\includegraphics[height=7px]{images/GeoPC}\hspace{2px} project:\\
%Dave A. May (ETHZ), Sascha M. Schnepp, Karl Rupp (TUW), Olaf Schenk (USI)
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title[\PETSc{} Tutorial]{Practical \PETSc{} Tutorial} 
\subtitle[]{}
\date{November 30, 2016}
% Long date messes up footer

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}
%\setbeamertemplate{section page}[customsection]
%\setbeamertemplate{subsection page}[customsubsection]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
\titlepage 
\begin{center}
\includegraphics[height=1.6em]{images/USI_ICS_Logo.jpg}
\hspace{8pt}
\raisebox{0.3em}{\includegraphics[height=1em]{images/eth_logo_kurz_pos.pdf}}
\hspace{4pt}
\includegraphics[height=1.6em]{images/pasc_avec}
\hspace{2pt}
\includegraphics[height=1.6em]{images/CSCS_RGB}
\end{center}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{frame}
%\tableofcontents 
%\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}[fragile]
\frametitle{This Tutorial}
  \begin{itemize}
      \item We will use PETSc as an extended example of working with a library
        \begin{itemize}
          \item This is because I am familiar with it, and because its flexible design allows us to examine many things quickly
            \item All libraries present limitations and annoyances, and no library is a silver bullet: PETSc is no exception
        \end{itemize}
  \item We don't have time for a full tutorial on the library, unfortunately
    \item I have adapted some ``miniapp'' code to use PETSc so that you can see familiar concepts in a new framework
    \item The majority of the time will be spent experimenting with this code: libraries allow you to quickly leverage functionality that would be time-consuming to ``roll yourself'', and PETSc is an extreme example of this in that you can even do this experimentation at runtime
      \item Please ask questions at any time.
  \end{itemize}

\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
\frametitle{What is \PETSc{}?}
  \begin{itemize}
      \item Origins as one of the first success stories of MPI, a library for domain decomposition-based PDE solvers
        \item Extended to provide a full set of tools for solving large-scale discretized PDE in distributed-memory parallel environments
        \item Distributed linear algebra, linear solvers, preconditioners, nonlinear solvers, timesteppers, domain management tools, optimization tools (TAO), and associated utilities.
  \item Over 20 years of development, fully supported, based at Argonne National Lab 
  \item Written in object-oriented C, with a Fortran interface \footnote{and see \texttt{petsc4py}, which offers a Python interface}
    \item Forms the basis for many of the libraries we've seen in the previous lecture, especially higher-level PDE libraries
  \end{itemize}
\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
\frametitle{Why Use \PETSc{}?}
  \begin{itemize}
      \item Write robust, scalable MPI codes to solve PDE, without writing much MPI code yourself
      \item Use a combinatorial explosion of solvers, configurable at runtime
      \item Run your code essentially anywhere, from your laptop to Piz Daint
      \item Configure with a huge number of external packages (including external linear solvers)
      \item Excellent support and community
      \item Open source, free software (2-clause BSD license)
  \end{itemize}
\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
\frametitle{What's in a Name?}
  \begin{itemize}
    \item \textbf{P}ortable
    \item \textbf{E}xtensible
    \item \textbf{T}oolkit for
    \item \textbf{S}cientific 
    \item \textbf{c}omputation
  \end{itemize}
  An alternate acronym: the ``\textbf{P}ortable, \textbf{E}xtensible \textbf{T}oolkit for \textbf{S}olver \textbf{c}omposition''

\end{frame}
% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
  \frametitle{Where to Learn More}
  \begin{itemize}
    \item The PETSc website \\ \href{http://www.mcs.anl.gov/petsc}{\url{mcs.anl.gov/petsc}}
    \item The manual \\ 
      \href{http://www.mcs.anl.gov/petsc/documentation/index.html}{\url{mcs.anl.gov/petsc/documentation/index.html}}
    \item Some simple hands-on tutorials \\ \href{http://www.mcs.anl.gov/petsc/documentation/tutorials/HandsOnExercise.html}{\url{mcs.anl.gov/petsc/documentation/tutorials/HandsOnExercise.html}}
    \item The mailing lists \\
      \href{http://www.mcs.anl.gov/petsc/miscellaneous/mailing-lists.html}{\url{mcs.anl.gov/petsc/miscellaneous/mailing-lists.html}}
    \item The man pages, and linked examples \\
      \href{http://www.mcs.anl.gov/petsc/documentation/index.html}{\url{mcs.anl.gov/petsc/documentation/index.html}}
  \end{itemize}
  

\end{frame}


% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
\frametitle{\PETSc{} Components}
  \includegraphics[width=\textwidth]{images/PetscTower.png}
\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
  \frametitle{\exercisecolor{ICS Exercise 1:} Hello World}
  Log on to the ICS Cluster and obtain the following example
  \begin{lstlisting}
git clone https://bitbucket.org/psanan/petsc_test_ics
  \end{lstlisting}
  Run it
  \begin{enumerate}
  \item On the login node
    \item With a batch script on the compute nodes
      \item In an interactive session
  \end{enumerate}

\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
\frametitle{\PETSc{} Miniapp}
  \begin{itemize}
    \item We will use a PDE used for the CSCS Summer School \footnote{\href{https://github.com/eth-cscs/SummerSchool2016}{\url{https://github.com/eth-cscs/SummerSchool2016}}}, Fischer's Equation in 2 dimensions
      $$
       \frac{\partial s}{\partial t} = D \left(\frac{\partial^2{s}}{\partial x^2} + \frac{\partial^2{s}}{\partial y^2}\right) + Rs(1-s)
      $$
      discretized the same way on the same domain, a rectangle equipped with a regular grid, using standard finite differences / finite volumes.
      \item We will solve the same system, but using \PETSc{}'s abstractions
      \item We will leverage a powerful design component of \PETSc{}, namely that it is \textbf{runtime configurable}, meaning that we can select from a huge number of solvers at runtime and quickly see how they perform
      \item We will write straightforward code which nevertheless will scale to large numbers of MPI processes (without us writing much MPI)
  \end{itemize}
\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
  \frametitle{Porting to PETSc}
 As a short example, code which applies a finite difference Laplacian to a vector (similar, but not identical, to what we do to port the miniapp)
  \begin{lstlisting}[language=C,basicstyle=\ttfamily\ttfamily\tiny]
#include <petscdmda.h>
#include "ctx.h"

#undef __FUNCT__
#define __FUNCT__ "applyA"
PetscErrorCode applyA(Mat A, Vec in, Vec out)
{
  PetscErrorCode    ierr;
  Ctx               *ctx; 
  const PetscScalar **inarr;
  PetscScalar       val,**outarr;
  PetscInt          i,j,ixs,iys,ixm,iym,imin,imax,jmin,jmax,M,N;
  PetscBool         left,right,up,down;
  Vec               in_local;
  PetscReal         oneoverhy2,oneoverhx2;

  PetscFunctionBeginUser;

  ierr = MatShellGetContext(A, &ctx);CHKERRQ(ierr);
  M           = ctx->M;
  N           = ctx->N;
  in_local    = ctx->work_local[0];
  oneoverhy2  = 1.0/(ctx->hy*ctx->hy); 
  oneoverhx2  = 1.0/(ctx->hx*ctx->hx);

  /* Scatter global-->local to have access to the required ghost values */
  ierr=DMGlobalToLocalBegin(ctx->da,in,INSERT_VALUES,in_local);CHKERRQ(ierr);
  ierr=DMGlobalToLocalEnd  (ctx->da,in,INSERT_VALUES,in_local);CHKERRQ(ierr);

  /* Continued ... */
}
  \end{lstlisting}

\end{frame}

\begin{frame}[fragile]
\frametitle{Porting to PETSc (continued)}

  \begin{lstlisting}[language=C,basicstyle=\ttfamily\tiny]
  /* Get the boundaries of the local subdomain */
  DMDAGetCorners(ctx->da, &ixs, &iys, 0, &ixm, &iym, 0);CHKERRQ(ierr);

  /* Get access to the raw arrays (with ghosts). 
     Note that PETSc allows these to be accessed with *global* indices */
  DMDAVecGetArray(ctx->da,out,&outarr);CHKERRQ(ierr);
  DMDAVecGetArrayRead(ctx->da,in_local,&inarr);CHKERRQ(ierr);

  /* Determine active (global) boundaries */
  up   = (iys       == 0); jmin = up   ? iys       + 1 : iys;
  down = (iys + iym == N); jmax = down ? iys + iym - 1 : iys + iym; 
  left = (ixs       == 0); imin = left ? ixs       + 1 : ixs; 
  right= (ixs + ixm == M); imax = right? ixs + ixm - 1 : ixs + ixm;

  /* Handle corners */
  if(up && left){
    val=0;
    j=0; i=0;
    val+=inarr[j  ][i+1] *       (-oneoverhx2);
    val+=inarr[j+1][i  ] *       (-oneoverhy2);
    val+=inarr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2);
    outarr[j][i]=val;
  }
  if(up && right){
    /* ... */
  }
  if(down && left){
    /* ... */
  }
  if(down && right){
    /* ... */
  }
  \end{lstlisting}
\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
\frametitle{Porting to PETSc (continued)}

  \begin{lstlisting}[language=C,basicstyle=\ttfamily\tiny]
  /* Handle edges (excluding corners ) */
  if (up){
    j=0;
    for (i=imin; i<imax; ++i) {
      val=0;
      val+=inarr[j  ][i-1] *       (-oneoverhx2);
      val+=inarr[j  ][i+1] *       (-oneoverhx2);
      val+=inarr[j+1][i  ] *       (-oneoverhy2);
      val+=inarr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2);
      outarr[j][i]=val;
    }
  }

  if (down){
    /* ... */
  }

  if (left){
    /* ... */
  }

  if (right){
    /* ... */
  }
\end{lstlisting}
\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
\frametitle{Porting to PETSc (continued)}

  \begin{lstlisting}[language=C,basicstyle=\ttfamily\tiny]
  /* Handle the interior points */
  for (j=jmin; j<jmax; ++j) {
    for (i=imin; i<imax; ++i) {
      val=0;
      val+=inarr[j-1][i  ] *       (-oneoverhy2);
      val+=inarr[j  ][i-1] *       (-oneoverhx2);
      val+=inarr[j  ][i+1] *       (-oneoverhx2);
      val+=inarr[j+1][i  ] *       (-oneoverhy2);
      val+=inarr[j  ][i  ] * 2.0 * (oneoverhy2 + oneoverhx2);
      outarr[j][i]=val;
    }
  }

  /* Revoke access to raw arrays */
  DMDAVecRestoreArray(ctx->da,out,&outarr);CHKERRQ(ierr);
  DMDAVecRestoreArrayRead(ctx->da,in_local,&inarr);CHKERRQ(ierr);

  PetscFunctionReturn(0);
}
  \end{lstlisting}
\end{frame}
% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
  \frametitle{\exercisecolor{Exercise 1:} Run the Code}
  \begin{columns}
    \column{0.5\textwidth}
  \begin{itemize}
  \item Obtain the miniapp code from the course directory or with 
    \begin{lstlisting}
    git clone https://bitbucket.org/psanan/ics_petsc_miniapp
    \end{lstlisting}
  \item Follow the instructions in \texttt{README.md}
    \item You should be able to produce an image like the one on the right
  \end{itemize}
    \column{0.5\textwidth}
    \includegraphics[width=\textwidth]{images/output.png}
  \end{columns}
  \vspace{10pt}
    {\small \textcolor{gray}{Aside: we are using the \texttt{petsc} module (thanks, Radim!), which frees us from having to configure and compile \PETSc{}. If you would like to use additional external packages, you can configure and build \PETSc{} yourself.}}
\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
  \frametitle{\walkthroughcolor{Code Walkthrough:} ODE Solver}
  \begin{itemize}
    \item \PETSc{} allows a ``top-down'' approach, so we begin by discussing how to adapt the miniapp to use \PETSc{} ODE/DAE solver object, called \texttt{TS}
      \item The advantages of doing things this way include
        \begin{itemize}
        \item The usual advantage of top-down design: having a clear view of the task, not implementing details until they are needed, etc.
        \item Additional flexibility, as you can use \PETSc{}'s objects for more of the code
        \end{itemize}
  \item Let's take a look at \texttt{main.c} 
  \end{itemize}
\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
  \frametitle{\exercisecolor{Exercise 2:} Changing the ODE solver}
  \begin{itemize}
    \item Run \texttt{./main -help} to see (many) possible command line options
  \item Experiment running the program
    \begin{itemize}
    \item With different numbers of grid points
    \item With different numbers of time steps
    \item With different numbers of MPI processes
  \end{itemize}
    \item Try the \texttt{-ts\_view} and \texttt{-ts\_monitor} options
    \item Try using different timesteppers, using \texttt{-ts\_type}.
      \begin{itemize}
        \item See the list of types in the \href{http://www.mcs.anl.gov/petsc/petsc-current/docs/manual.pdf}{manual} or on the man page for \texttt{TSType} at \href{http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/TS/TSType.html}{http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/TS/TSType.html}
        \item Not all will work immediately (some require you to provide more information about your system), but experiment with some standard choices like \texttt{rk}, \texttt{theta}, \texttt{ssp}, \texttt{bdf}, etc.
      \end{itemize}
      
  \end{itemize}
\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
  \frametitle{\walkthroughcolor{Code Walkthrough :} Distributed Vectors, Array, and Linear Operators}
  \begin{itemize}
  \item See \texttt{system.c}
    \item We will see how to assemble a Jacobian matrix
    \item \PETSc{} also supports custom ``matrix-free'' operators \footnote{See \href{http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/PC/MATSHELL.html}{\texttt{MatShell}}}, and can estimate the Jacobian for you using finite differences.
  \end{itemize}
\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
  \frametitle{\walkthroughcolor{Code Walkthrough :} Viewing}
  \begin{itemize}
  \item \PETSc{} objects can be ``viewed'' in various ways
    \item This includes writing to the screen, to files, or even to network sockets
    \item See \texttt{dump.c}
  \end{itemize}
\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
  \frametitle{\exercisecolor{Exercise 3:} Parallel Preconditioners}
\begin{itemize}
  \item Use the \texttt{-assemble} option 
  \item Using \texttt{-ts\_view}, determine what the default preconditioner (\texttt{PC}) is for the linear solver (\texttt{KSP})
  \item Use \texttt{-ksp\_monitor} and describe what happens to the convergence as you strong scale (increase the number of MPI ranks for the same problem size)
  \item Experiment with another preconditioner, an additive Schwarz method, with \texttt{-pc\_type asm}. Note that adding \texttt{-help} will now give you more options related to this preconditioner \footnote{The \texttt{-help} output can get long: try \texttt{./main -help -pc\_type asm | grep asm}}
\end{itemize}
\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
  \frametitle{\exercisecolor{Exercise 4:} Bigger Time Steps}
  \begin{itemize}
  \item  Experiment with command line options to increase the time step to, say, $1$
  \item Try to reduce the number of linear solver iterations by using a strong preconditioner like \texttt{-pc\_type gamg}\footnote{This is algebraic multigrid}
  \end{itemize}
  % One solution: try 
  % ./main -ts_view -ts_type beuler -ts_monitor  -assemble -pc_type gamg -ts_final_time 10 -dumpmatlab -nx 100 -ny 100 -options_left -ts_max_steps 1000000 -ts_dt 1
\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
\frametitle{Performance Profiling}
  \begin{itemize}
    \item \texttt{-log\_summary}\footnote{called \texttt{-log\_view} in more recent versions of \PETSc{}} provides a wealth of information, and is a necessary companion to allow quick interpertation of experiments with different solvers
    \item Includes
      \begin{itemize}
      \item Time and flops
      \item Call counts
      \item Load balances
      \item Cumulative memory usage (\textbf{Not} high-water mark)
      \end{itemize}
  \end{itemize}
\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
  \frametitle{\exercisecolor{Exercise 5:} Algorithmic Experimentation}
  \begin{itemize}
  \item Using only the command line options and \texttt{-log\_summary}, see how much you can speed up the code %TODO: see how I can do with this, and provide a reasonable problem size and see how I can do on daint
  \item Examine strong-scaling behavior (how does increasing the number of processes affect the solution time? 
  \end{itemize}
\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
  \frametitle{\exercisecolor{Exercise 6 (Bonus):} Writing a matrix-free operator application}
  \begin{itemize}
    \item Look at the documentation for \href{http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MATSHELL.html}{MatShell},\href{http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatShellSetOperation.html}{MatShellSetOperation()}, etc. (See the man pages, linked exampes, and Section 3.3 of the manual)
      \item Using the example from earlier in the slides as an example, write a matrix-free operator which applies the Jacobian in the miniapp, and confirm that it gives the same results as using the assembled operator. %TODO: prepare example of this
  \end{itemize}
\end{frame}

% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
\frametitle{Continuing with \PETSc{}}
  \begin{itemize}
    \item See the manual and other documentation at {\small \href{https://www.mcs.anl.gov/petsc/documentation/index.html}{\texttt{www.mcs.anl.gov/petsc/documentation/index.html}}}
    \item Learn how to use the mailing lists! See {\small \href{https://www.mcs.anl.gov/petsc/miscellaneous/mailing-lists.html}{\texttt{www.mcs.anl.gov/petsc/miscellaneous/mailing-lists.html}}}.
        \begin{itemize}
        \item Questions are answered quickly here
        \item General questions : \texttt{petsc-users}
        \item Developer topics (if planning to contribute) : \texttt{petsc-dev}
        \item Private queries, bugs, installation problems : \texttt{petsc-maint}
          \item For best results:
            \begin{enumerate}
              \item Include the \textbf{entire} error message
              \item Include \texttt{configure.log} and \texttt{make.log} if sending a configure/install problem to \texttt{petsc-maint}
                \item The more specific, the better. If you can provide code to reproduce your problem, that is best.
                  \item Note the archives
            \end{enumerate}
        \end{itemize}
      \item Note the large number of examples (see links from the man pages)
      \item Up-and-coming resource: \href{http://scicomp.stackexchange.com/}{scicomp.stackexchange.com} 
  \end{itemize}
\end{frame}


% --------------------------------------------------------------------------- %
\begin{frame}[fragile]
  \frametitle{\exercisecolor{ICS Exercise 2: Reconstructing an Example}}
  The aim is to translate the comments in a PETSc example back into PETSc code.
  \begin{lstlisting}
git clone https://bitbucket.org/psanan/ics_petsc_ex23_fillin.git
  \end{lstlisting}
  \begin{itemize}
    \item Fill in the \lstinline{/* TODO */}'s in the code, using the PETSc references mentioned above, and the help of the instructor.
      \item Run the code in parallel on the ICS cluster
        \item Try changing the preconditioner
        \item Run with \lstinline{-log_view} and see how your preconditioner affects the timing
  \end{itemize}

  (When finished, you should end up with something that resembles KSP tutorial example 23 from PETSc, at \href{ttp://www.mcs.anl.gov/petsc/petsc-3.5/src/ksp/ksp/examples/tutorials/ex23.c.html}{\url{ttp://www.mcs.anl.gov/petsc/petsc-3.5/src/ksp/ksp/examples/tutorials/ex23.c.html}}.)

  % ...

\end{frame}


\end{document}
