This repository holds the source for Patrick Sanan's CSCS summer school lectures 2016.

The C code for the PETSc tutorial (and rendered pdfs) live at 
  https://github.com/eth-cscs/SummerSchool2016

Update: Added version of these lectures (with more exercises) given at USI 2016.11.29-30
